<?php
require_once("config.php");
require_once("functions.php");
date_default_timezone_set($CONFIG['timezone']);
$db = new SQLite3($CONFIG['sqlite_file']);
?>

<!DOCTYPE html>
<head>
	<title>Picturemanager</title>
	<meta charset="utf-8">
	<meta name="generator" content="Bluefish 2.2.3" />
	<link rel="stylesheet" type="text/css" href="style.css" />
	<script type="text/javascript" src="jquery.js"></script>
</head>
<body>

<div id="navigationbox">
<ul id="navigation">
	<li id="logoitem"><a href="?p=home" id="logo"><img src="icons/logo.png" alt="Picturemanager" /></a></li>
	<li><a href="?p=exhibition_list" class="link"><div id="navlink">Ausstellungen &raquo;</div></a>
		<ul>
			<li><a href="?p=exhibition_add">Neue Ausstellung</a></li>
		</ul>
	</li>
	<li><a href="?p=picture_list"  class="link">Bilder &raquo;</a>
		<ul>
			<li><a href="?p=picture_add">Neues Bild</a></li>
		</ul>				
	</li>
	<li><a href="?p=person_list" class="link">Kontakte &raquo;</a>
		<ul>
			<li><a href="?p=person_edit">Neuer Kontakt</a></li>
		</ul>	
	</li>
</ul>
</div>

<?php
if(isset($_GET['p']))
{
	$file = "pages/".$_GET['p'] . ".php";
	if(file_exists($file))
		include($file);
	else
		echo "error 404";
}
else
{
	include("pages/home.php");
}


$db->close();
?>

</body>
</html>
