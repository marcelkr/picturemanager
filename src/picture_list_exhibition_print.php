<!DOCTYPE html>
<head>
	<title>Picturemanager -- Print Picture List<?php echo $CONFIG['pmversion'] ?></title>
	<meta charset="utf-8">
	<meta name="generator" content="Bluefish 2.2.4" />
	<link rel="stylesheet" type="text/css" href="picture_list_print.css" />
	<link rel="stylesheet" type="text/css" href="picture_list_print_print.css" media="print" />
	<script type="text/javascript" src="jquery.js"></script>
</head>

<script type="text/javascript">
$.urlParam = function(name){
    var results = new RegExp('[\\?&amp;]' + name + '=([^&amp;#]*)').exec(window.location.href);
    return results[1] || 0;
}

$(document).ready(function () {
	$('.upicon').click(function (){
		var exhibitionId = $.urlParam('exhibition_id');
		var exhibitedId = this.id;
		exhibitedId = exhibitedId.replace("up_","");
		$.ajax({
				type: "POST",
				url: "scripts/picture_exhibition_swap.php",
				data: {upSwapId: exhibitedId, exhibitionId: exhibitionId},
				success: function(response) {
					if(response == "OK") {
						$("#tr_"+exhibitedId).prev().before($("#tr_"+exhibitedId));
					}else{
						alert("Ein Fehler ist aufgetreten:\n" + response)
					}
				}
		});
	});
});
	
$(document).ready(function () {
	$('.downicon').click(function (){
		var exhibitionId = $.urlParam('exhibition_id');
		var exhibitedId = this.id;
		exhibitedId = exhibitedId.replace("down_","");
		$.ajax({
				type: "POST",
				url: "scripts/picture_exhibition_swap.php",
				data: {downSwapId: exhibitedId, exhibitionId: exhibitionId},
				success: function(response) {
					if(response == "OK") {
						$("#tr_"+exhibitedId).next().after($("#tr_"+exhibitedId));
					}else{
						alert("Ein Fehler ist aufgetreten:\n" + response)
					}
				}
		});
	});
});

$(document).ready(function () {
	$('#biggerThumbs').click(function (){
		$('.thumbnail').css("max-height","+=2mm");
		$('.thumbnail').css("max-width","+=2mm");
	});
});

$(document).ready(function () {
	$('#smallerThumbs').click(function (){
		$('.thumbnail').css("max-height","-=2mm");
		$('.thumbnail').css("max-width","-=2mm");
	});
});
</script>

<body>

<div class="noprint">
Bilder: 
	<img src="icons/loupe_bigger.png" class="loupeicon" id="biggerThumbs" alt="Vorschaubilder vergrößern" title="Vorschaubilder vergrößern" />
	<img src="icons/loupe_smaller.png" class="loupeicon" id="smallerThumbs" alt="Vorschaubilder verkleinern" title="Vorschaubilder verkleinern"/><br />

<hr />
</div>

<?php
require_once("config.php");
date_default_timezone_set($CONFIG['timezone']);
$db = new SQLite3($CONFIG['sqlite_file']);

$sql = "SELECT picture.id AS picid, picture.name AS name, exhibited.price AS price, exhibited.id AS id, exhibited.sortid AS sortid "
		."FROM picture JOIN exhibited "
		."ON exhibited.exhibition_id='".$_GET['exhibition_id']."' AND exhibited.picture_id=picture.id "
		."ORDER BY exhibited.sortid DESC";
$pictureQuery = $db->query($sql);
$exhibitionQuery = $db->query("SELECT * FROM exhibition WHERE id='".$_GET['exhibition_id']."'");
$exhibition = $exhibitionQuery->fetchArray();

if (!$pictureQuery) {
	echo "Anfrage konnte nicht ausgeführt werden: " . $db->lastErrorMsg();
	exit;
}
	
echo '<h1>Preisliste – '.$exhibition['title'].'</h1>';
echo '<h2></h2>';
echo '<p id="dateinfo">';
echo '<b>Beginn:</b> '.date("d.m.Y",$exhibition['date_from']);
if ($exhibition['date_to'] != 0)
	echo ' <b>Ende:</b> '.date("d.m.Y",$exhibition['date_to']);
echo '</p>';
?>

<table>
	<tr class="table_head">
		<td></td>
		<td></td>
		<td>Titel</td>
		<td>Preis [€]</td>
		<td class="noprint">Optionen</td>
	</tr>
<?php
//Hauptschleife
$i=1;
while ($picture = $pictureQuery->fetchArray()) {
	echo '<tr id="tr_'.$picture['id'].'">';
	echo '<td class="number">'.$i.'</td>';
	echo '<td><img src="thumbs/'.$picture['picid'].'.jpg" class="thumbnail" title="'.$picture['name'].'" alt="'.$picture['name'].'" /></td>';
	echo '<td><b>'.$picture['name'].'</b></td>';
	echo "<td>".$picture['price']."</td>";
	echo '<td class="noprint">';
	echo '<img src="icons/up.png" title="Hoch" alt="Hoch" class="upicon" id="up_'.$picture['id'].'" />';
	echo '<img src="icons/down.png" title="Herunter" alt="Herunter" class="downicon" id="down_'.$picture['id'].'" />';
	echo '</td>';
	echo '</tr>';
	$i++;
}
$db->close();
?>
</table>

<p id="contactinfo">
Christine Krüger &bull; Tel.: 0160 800 11 79 &bull; eMail: c.krueger@christine-krueger.net<br />
Schillerstr. 62 &bull; 76297 Stutensee
</p>

</body>
</html>
