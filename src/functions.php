<?php
function createthumb($w_max, $h_max, $sourcepath, $destpath) {
	$quality = 90;
	
	$imagesize = getimagesize($sourcepath);
	$src_h = $imagesize[1];
	$src_w = $imagesize[0];
	
	if($src_h > $src_w){
		$factor = $h_max / $src_h;
	}else{
		$factor = $w_max / $src_w;
	}
	
	$dst_w = $factor * $src_w;
	$dst_h = $factor * $src_h;
	
	$src_image = imagecreatefromjpeg($sourcepath);
	$new_image = imagecreatetruecolor($dst_w,$dst_h);
	$imgcopy = imagecopyresized($new_image, $src_image, 0, 0, 0, 0, $dst_w, $dst_h, $src_w, $src_h);
	$imgjpeg = imagejpeg($new_image, $destpath, $quality);
	
	if($new_image && $src_image && $imgcopy && $imgjpeg)
		return true;
	else
		return false;
}
?>