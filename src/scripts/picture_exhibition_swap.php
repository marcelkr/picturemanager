<?php
require_once("../config.php");
$db = new SQLite3("../".$CONFIG['sqlite_file']);

if($_SERVER['REQUEST_METHOD'] == "POST") {
	if(isset($_POST['upSwapId'])) {
		$exhibitedId = $_POST['upSwapId'];
		$maxSortId = $db->querySingle("SELECT sortid FROM exhibited WHERE sortid=(SELECT MAX(sortid) FROM exhibited WHERE exhibition_id='".$_POST['exhibitionId']."')");
		$currentSortId = $db->querySingle("SELECT sortid FROM exhibited WHERE id='".$exhibitedId."'");
	
		if($currentSortId < $maxSortId) {
			$nextExhibitedId = $db->querySingle("SELECT id FROM exhibited WHERE sortid > ".$currentSortId." ORDER BY sortid ASC LIMIT 1");
			$nextSortId = $db->querySingle("SELECT sortid FROM exhibited WHERE sortid > ".$currentSortId." ORDER BY sortid ASC LIMIT 1");
			
			$sortQuery1 = $db->query("UPDATE exhibited SET sortid='".$nextSortId."' WHERE id='".$exhibitedId."'");	
			$sortQuery2 = $db->query("UPDATE exhibited SET sortid='".$currentSortId."' WHERE id='".$nextExhibitedId."'");
			
			if ($sortQuery1 && $sortQuery2) {
				echo "OK";
			} else {
				echo $db->lastErrorMsg();
			}
		} else {
			echo "Oberes Ende erreicht";
		}
	} elseif(isset($_POST['downSwapId'])) {
		$exhibitedId = $_POST['downSwapId'];
		$minSortId = $db->querySingle("SELECT sortid FROM exhibited WHERE sortid=(SELECT MIN(sortid) FROM exhibited WHERE exhibition_id='".$_POST['exhibitionId']."')");
		$currentSortId = $db->querySingle("SELECT sortid FROM exhibited WHERE id='".$exhibitedId."'");
		
		if($currentSortId > $minSortId) {
			$prevExhibitedId = $db->querySingle("SELECT id FROM exhibited WHERE sortid < ".$currentSortId." ORDER BY sortid DESC LIMIT 1");
			$prevSortId = $db->querySingle("SELECT sortid FROM exhibited WHERE sortid < ".$currentSortId." ORDER BY sortid DESC LIMIT 1");
			
			$sortQuery1 = $db->query("UPDATE exhibited SET sortid='".$prevSortId."' WHERE id='".$exhibitedId."'");	
			$sortQuery2 = $db->query("UPDATE exhibited SET sortid='".$currentSortId."' WHERE id='".$prevExhibitedId."'");
			
			if ($sortQuery1 && $sortQuery2) {
				echo "OK";
			} else {
				echo $db->lastErrorMsg();
			}
		} else {
			echo "Unteres Ende erreicht";
		}
	} else {
		echo "Missing input Parameters (2)";
	}
	
} else {
	echo "Missing input Parameters (1)";
}

$db->close();
?>