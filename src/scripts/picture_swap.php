<?php
require_once("../config.php");
$db = new SQLite3("../".$CONFIG['sqlite_file']);

if($_SERVER['REQUEST_METHOD'] == "POST") {
	if(isset($_POST['upSwapId'])) {
		$currentRowId = $_POST['upSwapId'];
		$maxSortId = $db->querySingle("SELECT sortid FROM picture WHERE sortid=(SELECT MAX(sortid) FROM picture)");
		$currentSortId = $db->querySingle("SELECT sortid FROM picture WHERE id='".$currentRowId."'");
	
		if($currentSortId < $maxSortId) {
			$nextRowId = $db->querySingle("SELECT id FROM picture WHERE sortid > ".$currentSortId." ORDER BY sortid ASC LIMIT 1");
			$nextSortId = $db->querySingle("SELECT sortid FROM picture WHERE sortid > ".$currentSortId." ORDER BY sortid ASC LIMIT 1");
			
			$sortQuery1 = $db->query("UPDATE picture SET sortid='".$nextSortId."' WHERE id='".$currentRowId."'");	
			$sortQuery2 = $db->query("UPDATE picture SET sortid='".$currentSortId."' WHERE id='".$nextRowId."'");
			
			if ($sortQuery1 && $sortQuery2) {
				echo "OK";
			} else {
				echo $db->lastErrorMsg();
			}
		} else {
			echo "Oberes Ende erreicht";
		}
	} elseif(isset($_POST['downSwapId'])) {
		$currentRowId = $_POST['downSwapId'];
		$minSortId = $db->querySingle("SELECT sortid FROM picture WHERE sortid=(SELECT MIN(sortid) FROM picture)");
		$currentSortId = $db->querySingle("SELECT sortid FROM picture WHERE id='".$currentRowId."'");
		
		if($currentSortId > $minSortId) {
			$prevRowId = $db->querySingle("SELECT id FROM picture WHERE sortid < ".$currentSortId." ORDER BY sortid DESC LIMIT 1");
			$prevSortId = $db->querySingle("SELECT sortid FROM picture WHERE sortid < ".$currentSortId." ORDER BY sortid DESC LIMIT 1");
			
			$sortQuery1 = $db->query("UPDATE picture SET sortid='".$prevSortId."' WHERE id='".$currentRowId."'");	
			$sortQuery2 = $db->query("UPDATE picture SET sortid='".$currentSortId."' WHERE id='".$prevRowId."'");
			
			if ($sortQuery1 && $sortQuery2) {
				echo "OK";
			} else {
				echo $db->lastErrorMsg();
			}
		} else {
			echo "Unteres Ende erreicht";
		}
	} else {
		echo "Missing input Parameters (2)";
	}
	
} else {
	echo "Missing input Parameters (1)";
}

$db->close();
?>