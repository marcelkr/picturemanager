<?php
require_once("../config.php");
$db = new SQLite3("../".$CONFIG['sqlite_file']);

if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['id'])) {
	$id = $_POST['id'];
	$pictureQuery = $db->query("SELECT id,name FROM picture WHERE id='".$id."'");	
	$picture = $pictureQuery->fetchArray();
	
	if(file_exists("../pictures/".$picture['id'].".jpg"))
		$unlinkpic = unlink("../pictures/".$id.".jpg");
	if(file_exists("../thumbs/".$picture['id'].".jpg"))
		$unlinkthumb = unlink("../thumbs/".$id.".jpg");
	
	$del1 = $db->query("DELETE FROM picture WHERE id='".$id."'");
	$del2 = $db->query("DELETE FROM exhibited WHERE picture_id='".$id."'");
	
	if ($del1 && $del2) {
		echo "OK";
	} else {
		echo $db->lastErrorMsg();
	}
} else {
	echo "Missing input Parameters";
}

$db->close();
?>