<?php
$CONFIG['sqlite_file'] = "picturemanager.sqlite";
$CONFIG['timezone'] = "Europe/Berlin";

$CONFIG['upload_mimetype'] = "image/jpeg";
$CONFIG['upload_maxsize'] = 10485760;
?>
