<?php
header("Content-type: application/force-download");
header("Content-Disposition: filename=export.csv");
header("Content-Description: Downloaded File");

require_once("config.php");
$db = new SQLite3($CONFIG['sqlite_file']);

if(isset($_GET['person_list'])) {
echo '"Kategorie","Nachname","Vorname","PLZ","Ort","Strasse"
';
	if(!isset($_GET['exhibition_id'])) {
		$result = $db->query("SELECT * FROM person");
		while ($person = $result->fetchArray()) {
echo '"'.$person['category'].'","'.$person['last_name'].'","'.$person['first_name'].'","'.$person['zipcode'].'","'.$person['town'].'","'.$person['street'].'"
';
		}
	}else{
		$result = $db->query("SELECT person.* FROM invited JOIN person ON invited.person_id=person.id WHERE invited.exhibition_id=".$_GET['exhibition_id']);
		while ($row = $result->fetchArray()) {
			echo '"'.$person['category'].'","'.$person['last_name'].'","'.$person['first_name'].'","'.$person['zipcode'].'","'.$person['town'].'","'.$person['street'].'"
';
		}
	}
}

if(isset($_GET['picture_list'])) {
echo '"Name","Technik (BxH)","Groesse","Preis"
';
		$result = $db->query("SELECT picture.* FROM exhibited JOIN picture ON exhibited.picture_id=picture.id WHERE exhibited.exhibition_id='".$_GET['exhibition_id']."'");
		while ($picture = $result->fetchArray()) {
			echo '"'.$picture['name'].'","'.$picture['technique'].'","'.$picture['width'].' x '.$picture['height'].' cm","'.$picture['avails'].'"
';
		}
}


if(isset($_GET['picture_list_exhibited'])) {
echo '"Name","Technik (BxH)","Groesse","Preis"
';
	if(!isset($_GET['exhibition_id'])) {
		$result = $db->query("SELECT * FROM picture");
		while ($picture = $result->fetchArray()) {
echo '"'.$picture['name'].'","'.$picture['technique'].'","'.$picture['width'].' x '.$picture['height'].' cm","'.$picture['avails'].'"
';
		}
	}else{
		$result = $db->query("SELECT picture.* FROM exhibited JOIN picture ON exhibited.picture_id=picture.id WHERE exhibited.exhibition_id='".$_GET['exhibition_id']."'");
		while ($picture = $result->fetchArray()) {
echo '"'.$picture['name'].'","'.$picture['technique'].'","'.$picture['width'].' x '.$picture['height'].' cm","'.$picture['avails'].'"
';
		}
	}
}
$db->close();
?>
