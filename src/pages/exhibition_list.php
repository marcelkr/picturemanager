<script type="text/javascript">
$(document).ready(function () {
	$(".deleteicon").click(function() {
		var answer = confirm(unescape("Soll das Ausstellung unwiederrufbar gel%F6scht werden%3F"));
		var exhibitionid = this.id;
		if (answer){
			$.ajax({
				type: "POST",
				url: "scripts/exhibition_delete.php",
				data: {id: exhibitionid},
				success: function(response) {
					if(response == "OK") {
						$("#tr_" + exhibitionid).hide(800);
					}else{
						alert("Ein Fehler ist aufgetreten:\n" + response)
					}
				}
			});
		}
	});
});
</script>

<?php
if(isset($_GET['protect'])) {
	$result = $db->query("UPDATE exhibition SET protected=1 WHERE id=".$_GET['protect']);
	if (!$result) {
		echo '<p class="error">Anfrage konnte nicht ausgeführt werden:<br />' . $db->lastErrorMsg() . '</p>';
		exit;
	}
}

if(isset($_GET['unprotect'])) {
	$result = $db->query("UPDATE exhibition SET protected=0 WHERE id='".$_GET['unprotect']."'");
	if (!$result) {
		echo '<p class="error">Anfrage konnte nicht ausgefü-hrt werden:<br />' . $db->lastErrorMsg() . '</p>';
		exit;
	}
}
?>

<!--<p>
Zeige ausschließlich: <a href="index.php?p=exhibition_list">alle</a> &bull;
	<a href="index.php?p=exhibition_list&amp;ongoing">andauernde</a> &bull;
	<a href="index.php?p=exhibition_list&amp;over">vergangene</a>
</p>-->

<table>
	<tr class="table_head">
		<td><a href="?p=exhibition_list&amp;order=d">Datum</a></td>
		<td><a href="?p=exhibition_list&amp;order=l">Ort</a></td>
		<td>E/G</td>
		<td>Optionen</td>
	</tr>
	
		<?php
			/*if (isset($_GET['order'])) {
				switch ($_GET['order']) {
					case 'd': $order = "ORDER BY date_from,location.name"; break;
					case 'l': $order = "ORDER BY location.name,date_from"; break;
					default: $order = "ORDER BY date_from,location"; break;
				}
				$result = $db->query("SELECT * FROM exhibition ".$order);
			} else {*/
				$result = $db->query("SELECT * FROM exhibition ORDER BY date_from");
			//}
			
			if (!$result) {
				echo "Anfrage konnte nicht ausgeführt werden: " . $db->lastErrorMsg();
				exit;
			}
			
			if ($db->querySingle("SELECT COUNT(*) FROM exhibition") == 0) {
				echo "Keine Einträge vorhanden";
				exit;
			}
			
			while ($exhibition = $result->fetchArray()) {
				$number_pictures = $db->querySingle("SELECT COUNT(*) FROM exhibited WHERE exhibition_id='".$exhibition['id']."'");
				
				$number_persons = $db->querySingle("SELECT COUNT(*) FROM invited WHERE exhibition_id='".$exhibition['id']."'");
				
				echo '<tr id="tr_'.$exhibition['id'].'">';
					
				if($exhibition['date_to'] == 0)
					echo '<td>'.date("d.m.Y",$exhibition['date_from']).' - ?</td>';
				else
					echo '<td>'.date("d.m.Y",$exhibition['date_from']).' - '.date("d.m.Y",$exhibition['date_to']).'</td>';
				
				echo '<td>'.$exhibition['location'].' <b>"'.$exhibition['title'].'"</b></td>';
				
				if($exhibition['solo'])
					echo '<td><img src="icons/solo.png" alt="Einzelausstellung" /></td>';
				else
					echo '<td><img src="icons/group.png" alt="Gruppenausstellung" /> '.$exhibition['others'].'</td>';
					
				echo '<td><a href="?p=picture_list_exhibited&amp;exhibition_id='.$exhibition['id'].'">Bilder ('.$number_pictures.')</a> ';
				echo '<a href="?p=person_list&amp;exhibition_id='.$exhibition['id'].'">Einl. ('.$number_persons.')</a> ';
				
				if($exhibition['protected']==1) {
					echo '<a href="?p=exhibition_list&amp;unprotect='.$exhibition['id'].'"><img src="icons/unprotect.png" class="protecticon" title="Schutz aufheben" alt="Schutz aufheben" /></a></td>';
				} else {
					echo '<a href="?p=exhibition_edit&amp;edit='.$exhibition['id'].'"><img src="icons/edit.png" class="editicon" title="Bearbeiten" alt="Bearbeiten" /></a> ';
					echo '<img src="icons/delete.png" title="Löschen" alt="Löschen" class="deleteicon" id="'.$exhibition['id'].'" />';
					echo '<a href="?p=exhibition_list&amp;protect='.$exhibition['id'].'"><img src="icons/protect.png" class="protecticon" title="Sch&uuml;tzen" alt="Sch&uuml;tzen" /></a></td>';
				}
				echo "</tr>";
			}
		?>
</table>
