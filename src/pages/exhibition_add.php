<script type="text/javascript">
$(document).ready(function(){
	$(this).find("input:radio[name='solo']").click(function() {
		if( $("input:radio:checked[name='solo']").val() == "solo" ) {
			$('.group_exhibition_info').hide(0);
		} else {
			$('.group_exhibition_info').show(0);
		}
	});
});

if( $("input:radio:checked[name='solo']").val() == "solo" ) {
	$('.group_exhibition_info').hide(0);
} else {
	$('.group_exhibition_info').show(0);
}
</script>

<?php
if($_SERVER['REQUEST_METHOD'] == "POST") {
	$date1 = strtotime($_POST['date1']);
	$date2 = strtotime($_POST['date2']);
	
	if($_POST['solo'] == "solo")
		$solo = 1;
	else
		$solo = 0;
	
	$result = $db->query("INSERT INTO exhibition (location,title,solo,others,contact,phone,email,date_from,date_to,protected) VALUES ('".$_POST['location']."','".$_POST['title']."','".$_POST['solo']."','".$_POST['others']."','".$_POST['contact']."','".$_POST['phone']."','".$_POST['email']."','".$date1."','".$date2."', '0')");
	if ($result) {
		echo '<p class="done">Ausstellung eingetragen</p>';
	}else{
		echo '<p class="error">Ausstellung konnte nicht eingetragen werden:<br />' . $db->lastErrorMsg() . '</p>';
		exit;
	}
}
?>

<h2>Neue Ausstellung</h2>
<form action="?p=exhibition_add" method="post">
	<fieldset>
		<legend>Ausstellung</legend>
		
		<label>Titel *</label>
		<input name="title" type="text" required autofocus />
		
		<label>Datum von</label>
		<input name="date1" type="date" />
		<label>Datum bis</label>
		<input name="date2" type="date" placeholder="leer, wenn unbekannt" />
		
		<label>Veranstaltungsort</label>
		<input name="location" type="text" />
		
		<input type="radio" name="solo" value="solo" id="solo" checked /> <label class="radiolabel" for="solo">Einzelausstellung</label><br />
		<input type="radio" name="solo" value="group" id="group" /> <label class="radiolabel" for="group">Gruppenausstellung</label>
		
		
		<label class="group_exhibition_info">Weitere Teilnehmer</label>
		<input name="others" class="group_exhibition_info" type="text" />
	</fieldset>
	
	<fieldset>
		<legend>Ansprechpartner</legend>
		
		<label >Name</label>
		<input name="contact" type="text" />
		
		<label>Telefon</label>
		<input name="phone" id="phone" type="tel" />
		
		<label>E-Mail</label>
		<input name="email" type="email" />
	</fieldset>
	
	<fieldset>
		<input type="submit" name="submit_new" value="Speichern" />
	</fieldset>
</form>