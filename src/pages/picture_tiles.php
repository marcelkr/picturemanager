<script type="text/javascript">
$(document).ready(function () {
	$(".deleteicon").click(function() {
		var answer = confirm(unescape("Soll das Bild unwiederrufbar gel%F6scht werden%3F"));
		var picid = this.id;
		picid = picid.replace("del_","");
		if (answer){
			$.ajax({
				type: "POST",
				url: "scripts/picture_delete.php",
				data: {id: picid},
				success: function(response) {
					if(response == "OK") {
						$("#li_" + picid).hide(800);
					}else{
						alert("Ein Fehler ist aufgetreten:\n" + response)
					}
				}
			});
		}
	});
});

$(document).ready(function () {
	$('.upicon').click(function (){
		var picid = this.id;
		picid = picid.replace("up_","");
		$.ajax({
				type: "POST",
				url: "scripts/picture_swap.php",
				data: {upSwapId: picid},
				success: function(response) {
					if(response == "OK") {
						$("#li_"+picid).prev().before($("#li_"+picid));
					}else{
						alert("Ein Fehler ist aufgetreten:\n" + response)
					}
				}
		});
	});
});
	
$(document).ready(function () {
	$('.downicon').click(function (){
		var picid = this.id;
		picid = picid.replace("down_","");
		$.ajax({
				type: "POST",
				url: "scripts/picture_swap.php",
				data: {downSwapId: picid},
				success: function(response) {
					if(response == "OK") {
						$("#li_"+picid).next().after($("#li_"+picid));
					}else{
						alert("Ein Fehler ist aufgetreten:\n" + response)
					}
				}
		});
	});
});

$(document).ready(function () {
	$('#biggerThumbs').click(function (){
		$('.thumbnail').css("max-height","+=50px");
		$('.thumbnail').css("max-width","+=50px");
	});
});

$(document).ready(function () {
	$('#smallerThumbs').click(function (){
		$('.thumbnail').css("max-height","-=50px");
		$('.thumbnail').css("max-width","-=50px");
	});
});
</script>

<p>
Zeige ausschließlich: <a href="index.php?p=picture_list">alle</a> &bull;
	<!--<a href="index.php?p=picture_list&amp;onlyexhibited">ausgestellte</a> &bull;
	<a href="index.php?p=picture_list&amp;notexhibited">nicht ausgestellte</a> &bull;-->
	<a href="index.php?p=picture_list&amp;sold">verkaufte</a> &bull;
	<a href="index.php?p=picture_list&amp;notsold">nicht verkaufte</a> &bull;
	<a href="index.php?p=picture_list&amp;givenaway">verschenkte</a><br />
Exportiere: <a href="export.php?picture_list">alle</a><br />
Vorschaubilder: 
	<img src="icons/loupe_bigger.png" class="loupeicon" id="biggerThumbs" alt="Vorschaubilder vergrößern" title="Vorschaubilder vergrößern" />
	<img src="icons/loupe_smaller.png" class="loupeicon" id="smallerThumbs" alt="Vorschaubilder verkleinern" title="Vorschaubilder verkleinern"/><br />
Ansicht: 
	<a href="index.php?p=picture_list"><img src="icons/list.png" class="listicon" alt="Listenansicht" /></a> 
	<a href="index.php?p=picture_tiles"><img src="icons/tiles.png" class="tilesicon" alt="Kachelansicht"/></a>
</p>

<?php
if(isset($_GET['onlyexhibited'])) {
	$sql = "SELECT picture.id AS id, picture.sold AS sold, picture.name AS name, picture.width AS width, picture.height AS height, picture.technique AS technique, picture.year AS year, picture.buyer_phone AS buyer_phone, picture.buyer_name AS buyer_name, picture.buyer_email AS buyer_email, picture.buyer_town AS buyer_town, picture.avails AS avails, exhibition.date_from, exhibition.date_to\n"
    		. "FROM picture "
    		. "JOIN exhibited ON picture.id = exhibited.picture_id "
    		. "JOIN exhibition ON exhibition.id = exhibited.exhibition_id "
    		. "WHERE (exhibition.date_from <= strftime('%s','now') "
    		. "AND exhibition.date_to >= strftime('%s','now')) "
    		. "OR exhibition.date_to = '0'"
    		. "ORDER BY sortid  DESC";
} else if (isset($_GET['notexhibited'])) {
	$sql = "SELECT picture.id AS id, picture.sold AS sold, picture.name AS name, picture.width AS width, picture.height AS height, picture.technique AS technique, picture.year AS year, picture.buyer_phone AS buyer_phone, picture.buyer_name AS buyer_name, picture.buyer_email AS buyer_email, picture.buyer_town AS buyer_town, picture.avails AS avails, exhibition.date_from, exhibition.date_to\n"
    		. "FROM picture\n"
    		. "JOIN exhibited ON picture.id = exhibited.picture_id "
    		. "JOIN exhibition ON exhibition.id = exhibited.exhibition_id "
    		. "WHERE (exhibition.date_from <= strftime('%s','now') "
    		. "AND exhibition.date_to <= strftime('%s','now'))"
    		. "ORDER BY sortid  DESC";
} else if (isset($_GET['sold'])) {
	$sql = "SELECT * FROM picture WHERE sold != 0 and avails != 0 ORDER BY sortid DESC";
} else if (isset($_GET['notsold'])) {
	$sql = "SELECT * FROM picture WHERE sold = 0 ORDER BY sortid DESC";
} else if (isset($_GET['givenaway'])) {
	$sql = "SELECT * FROM picture WHERE sold = 1 AND avails = 0 ORDER BY sortid DESC";
} else {
	$sql = "SELECT * FROM picture ORDER BY sortid DESC";
}
$pictureQuery = $db->query($sql);

if (!$pictureQuery) {
	echo "Anfrage konnte nicht ausgeführt werden: " . $db->lastErrorMsg();
	exit;
}

if ($db->querySingle("SELECT COUNT(*) FROM picture") == 0) {
	echo "Keine Zeilen gefunden, nichts auszugeben, daher Abbruch";
	exit;
}

echo '<ul class="picturetile_outer">';
//Hauptschleife
while ($picture = $pictureQuery->fetchArray()) {
	//echo '<div class="picturetile">';
	echo '<li id="li_'.$picture['id'].'">';
		echo '<ul class="picturetile_inner">';
			echo '<li>';
				if(file_exists("thumbs/".$picture['id'].".jpg"))
					echo '<a href="pictures/'.$picture['id'].'.jpg"><img src="thumbs/'.$picture['id'].'.jpg" class="thumbnail" title="'.$picture['name'].'" alt="'.$picture['name'].'" /></a>';
				else
					echo '<img src="icons/questionmark.png" class="thumbnail" title="Kein Bild hochgeladen" alt="Kein Bild hochgeladen" />';
			echo '</li>';
			echo '<li>';			
			if($picture['sold'] == true)
				echo '<img src="icons/reddot.png" class="reddot" alt="verkauft" /> ';
			echo '<b>'.$picture['name'].'</b>';
			echo '</li>';
			echo '<li>';		
				echo '<ul class="controlsList">';
					echo '<li><a href="?p=picture_edit&amp;edit='.$picture['id'].'"><img src="icons/edit.png" class="editicon" title="Bearbeiten" alt="Bearbeiten" /></a></li>';
					echo '<li><img src="icons/delete.png" title="Löschen" alt="Löschen" class="deleteicon" id="del_'.$picture['id'].'" /></li>';
					echo '<li><img src="icons/up.png" title="Hoch" alt="Hoch" class="upicon" id="up_'.$picture['id'].'" /></li>';
					echo '<li><img src="icons/down.png" title="Herunter" alt="Herunter" class="downicon" id="down_'.$picture['id'].'" /></li>';
				echo '</ul>';
			echo '</li>';	
		echo '</ul>';
	//echo '</div>';	
	echo '</li>';
}
echo "</ul>";
?>