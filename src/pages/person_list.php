<?php
if(isset($_GET['delete'])) {
	if(!isset($_GET['rlydel'])) {
		echo '<p class="question">Datensat wirklich l&ouml;schen? ';
		echo '<a href="?p=person_list&amp;delete='.$_GET['delete'].'&amp;rlydel=1">Ja</a> <a href="?p=person_list">Nein</a></p>';
	} else {
		$result = $db->query("DELETE FROM person WHERE id='".$_GET['delete']."'");
		if ($result) {
			echo '<p class="done">Datensatz gelöscht</p>';
		} else {
			echo '<p class="error">Anfrage konnte nicht ausgeführt werden: ' . $db->lastErrorMsg() . '</p>';
			exit;
		}
	}
}

if(isset($_GET['invite'])) {
	$rowCount = $db->querySingle("SELECT COUNT(*) FROM invited WHERE person_id='".$_GET['invite']."' AND exhibition_id='".$_GET['exhibition_id']."'");
	if($rowCount == 0) {	// the person is not invited yet
		$result = $db->query("INSERT INTO invited (exhibition_id ,person_id) VALUES ('".$_GET['exhibition_id']."', '".$_GET['invite']."')");
		if (!$result) {
			echo '<p class="error">Anfrage konnte nicht ausgef&uuml;hrt werden: ' . $db->lastErrorMsg() . '</p>';
			exit;
		}
	}
}

if(isset($_GET['uninvite'])) {
	$result = $db->query("DELETE FROM invited WHERE person_id='".$_GET['uninvite']."' AND exhibition_id='".$_GET['exhibition_id']."'");
	if (!$result) {
		echo '<p class="error">Anfrage konnte nicht ausgef&uuml;hrt werden: ' . $db->lastErrorMsg() . '</p>';
		exit;
	}
}

// ### exhibition mode ##############################################################################################
if(isset($_GET['exhibition_id'])) {
	echo "<p>".$db->querySingle("SELECT COUNT(*) FROM invited WHERE exhibition_id=".$_GET['exhibition_id'])." Personen eingeladen</p>";
	
	$result = $db->query("SELECT protected FROM exhibition WHERE id='".$_GET['exhibition_id']."'");
	$row = $result->fetchArray();
	$protected = $row['protected'];
}
?>

<p>
<a href="export.php?person_list">Exportiere alle</a>
</p>

<table>
	<tr class="table_head">
		<td><a href="?p=person_list&amp;order=c">Kategorie</a></td>
		<td><a href="?p=person_list&amp;order=l">Nachname</a></td>
		<td><a href="?p=person_list&amp;order=f">Vorname</a></td>
		<td><a href="?p=person_list&amp;order=s">Straße</a></td>
		<td><a href="?p=person_list&amp;order=t">Ort</a></td>
		<td>E-Mail</td>
		<td>Telefon</td>
		<td>Optionen</td>
	</tr>
	
		<?php
			if (isset($_GET['order'])) {
				switch ($_GET['order']) {
					case 'l': $order = " ORDER BY last_name"; break;
					case 'f': $order = " ORDER BY first_name"; break;
					case 'c': $order = " ORDER BY category"; break;
					case 's': $order = " ORDER BY street"; break;
					case 't': $order = " ORDER BY zipcode"; break;
					default: $order = " ORDER BY category,last_name"; break;
				}
				$result = $db->query("SELECT * FROM person".$order);
			} else {
				$result = $db->query("SELECT * FROM person ORDER BY category,last_name");
			}
			
			if (!$result) {
				echo "Anfrage konnte nicht ausgeführt werden: " . $db->lastErrorMsg();
				exit;
			}
			
			if ($db->querySingle("SELECT COUNT(*) FROM person") == 0) {
				echo "Keine Einträge vorhanden";
				exit;
			}
			
			while ($row = $result->fetchArray()) {
				if(isset($_GET['exhibition_id'])) {
					$rowCount = $db->querySingle("SELECT COUNT(*) FROM invited WHERE person_id=".$row['id']." AND exhibition_id=".$_GET['exhibition_id']);
						
					if($rowCount != 0) {
						$invited = true;
						echo '<tr class="tr_green">';
					} else {
						$invited = false;
						echo '<tr>';
					}
				} else {
					$invited = false;
					echo "<tr>";
				}
				
				echo "<td>".$row['category']."</td>";
				echo "<td>".$row['last_name']."</td>";
				echo "<td>".$row['first_name']."</td>";
				echo "<td>".$row['street']."</td>";
				echo "<td>".$row['zipcode']." ".$row['town']." (".$row['country'].")</td>";
				echo "<td>".$row['email']."</td>";
				echo "<td>".$row['phone']."</td>";
				echo "<td>";
				if(isset($_GET['exhibition_id'])) {
					if(!$protected) {
						if($invited)
							echo '<a href="?p=person_list&amp;exhibition_id='.$_GET['exhibition_id'].'&amp;uninvite='.$row['id'].'">ausladen</a>';
						else
							echo '<a href="?p=person_list&amp;exhibition_id='.$_GET['exhibition_id'].'&amp;invite='.$row['id'].'">einladen</a>';
					} else {
						echo '<img src="icons/protect.png" height="20" width="20" style="border: 0" title="Ausstellung gesch&uuml;tzt, zum bearbeiten in der Ausstellungsliste den Schutz entfernen" alt="Gesch&uuml;tzt" />';
					}
				} else {
					echo '<a href="?p=person_edit&amp;edit='.$row['id'].'"><img src="icons/edit.png" height="20" width="20" style="border: 0" title="Bearbeiten" alt="Bearbeiten" /></a> <a href="?p=person_list&amp;delete='.$row['id'].'"><img src="icons/delete.png" height="20" width="20" style="border: 0" title="L&ouml;schen" alt="L&ouml;schen" /></a>';
				}
				echo "</td>";
				echo "</tr>";
			}
		?>
</table>
