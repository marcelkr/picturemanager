<script type="text/javascript">
$(document).ready(function(){
	$(this).find("input:radio[name='solo']").click(function() {
		if( $("input:radio:checked[name='solo']").val() == "solo" ) {
			$('.group_exhibition_info').hide(0);
		} else {
			$('.group_exhibition_info').show(0);
		}
	});
});

$(document).ready(function(){
	if( $("input:radio:checked[name='solo']").val() == "solo" ) {
		$('.group_exhibition_info').hide(0);
	} else {
		$('.group_exhibition_info').show(0);
	};
});
</script>

<?php
if($_SERVER['REQUEST_METHOD'] == "POST") {
	$date1 = strtotime($_POST['date1']);
	$date2 = strtotime($_POST['date2']);
	
	if($_POST['solo'] == "solo")
		$solo = 1;
	else
		$solo = 0;
	
	$result = $db->query("UPDATE exhibition SET location='".$_POST['location']."', title='".$_POST['title']."', solo='".$solo."', others='".$_POST['others']."', contact='".$_POST['contact']."', phone='".$_POST['phone']."', email='".$_POST['email']."', date_from='".$date1."',date_to='".$date2."' WHERE id='".$_GET['edit']."'");
	if ($result) {
		echo '<p class="done">Ausstellung geändert</p>';
	}else{
		echo '<p class="error">Ausstellung konnte nicht geändert werden:<br />' . $db->lastErrorMsg() . '</p>';
		exit;
	}
}

$result = $db->query("SELECT * FROM exhibition WHERE id='".$_GET['edit']."'");
$exhibition = $result->fetchArray();
if(empty($exhibition['date_to'])) {
	$date_to = "";
} else {
	$date_to = date("d.m.Y",$exhibition['date_to']);
}	
?>

<h2>Ausstellung ändern</h2>
<form action="?p=exhibition_edit&amp;edit=<?php echo $_GET['edit'] ?>" method="post">
	<fieldset>
		<legend>Ausstellung</legend>
		
		<label>Titel *</label>
		<input name="title" type="text" value="<?php echo $exhibition['title'] ?>" required autofocus />
		
		<label>Datum von</label>
		<input name="date1" type="date" value="<?php echo date("Y-m-d",$exhibition['date_from']) ?>" />
		<label>Datum bis</label>
		<input name="date2" type="date" value="<?php echo date("Y-m-d",$exhibition['date_to']) ?>" />
		
		<label>Veranstaltungsort</label>
		<input name="location" type="text" value="<?php echo $exhibition['location'] ?>" />
		
		<?php
		if($exhibition['solo']) {
			echo '<input type="radio" name="solo" value="solo" id="solo" checked /> <label class="radiolabel" for="solo">Einzelausstellung</label><br />';
			echo '<input type="radio" name="solo" value="group" id="group" /> <label class="radiolabel" for="group">Gruppenausstellung</label>';
		} else {
			echo '<input type="radio" name="solo" value="solo" id="solo" /> <label class="radiolabel" for="solo">Einzelausstellung</label><br />';
			echo '<input type="radio" name="solo" value="group" id="group" checked /> <label class="radiolabel" for="group">Gruppenausstellung</label>';
		}
		?>
		
		<label class="group_exhibition_info">Weitere Teilnehmer</label>
		<input name="others" class="group_exhibition_info" type="text" value="<?php echo $exhibition['others'] ?>" />
	</fieldset>
	
	<fieldset>
		<legend>Ansprechpartner</legend>
		
		<label >Name</label>
		<input name="contact" type="text" value="<?php echo $exhibition['contact'] ?>" />
		
		<label>Telefon</label>
		<input name="phone" id="phone" type="tel" value="<?php echo $exhibition['phone'] ?>" />
		
		<label>E-Mail</label>
		<input name="email" type="email" value="<?php echo $exhibition['email'] ?>" />
	</fieldset>
	
	<fieldset>
		<input type="submit" name="submit_edit" value="Speichern" />
	</fieldset>
</form>
