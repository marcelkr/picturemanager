<?php
if($_SERVER['REQUEST_METHOD'] == "POST") {
	$result = $db->query("DELETE FROM exhibited WHERE exhibition_id='".$_GET['exhibition_id']."'");
	if(!$result) {
		echo '<p class="error">Bilder konnten nicht hinzugefügt werden:<br />' . $db->lastErrorMsg() . '</p>';
	} else {
		
		foreach ($_POST as $key => $data) {
			if($data == "on") {
				foreach ($_POST as $key2 => $data2) {
					if($key2 == "price_".$key) {
						$price = $data2;
					}
					if($key2 == "location_".$key) {
						$location = $data2;
					}
				}
				$result = $db->query("INSERT INTO exhibited (exhibition_id, picture_id, price,location) VALUES  ('".$_GET['exhibition_id']."','".$key."','".$price."','".$location."')");
				if(!$result){
					echo '<p class="error">Bilder konnten nicht hinzugefügt werden:<br />' . $db->lastErrorMsg() . '</p>';
					break;
				}
			}
		}
	}
}

$result = $db->query("SELECT * FROM exhibited WHERE exhibition_id='".$_GET['exhibition_id']."'");
$rowCount = $db->querySingle("SELECT COUNT(*) FROM exhibited WHERE exhibition_id='".$_GET['exhibition_id']."'");

$result2 = $db->query("SELECT * FROM exhibition WHERE id='".$_GET['exhibition_id']."'")->fetchArray();
$protected = $result2['protected'];

echo '<h2>Bilder hinzufügen zu: '.$result2['title'].'</h2>';
echo "<p><b>".$result2['location']."</b><br />".$rowCount." Bilder auf dieser Ausstellung</p>";
?>

<p>
Zeige ausschließlich: 
	<a href="index.php?p=picture_list_exhibited&amp;exhibition_id=<?php echo $_GET['exhibition_id'] ?>">alle</a>&bull;
	<a href="index.php?p=picture_list_exhibited&amp;exhibition_id=<?php echo $_GET['exhibition_id'] ?>&amp;onlyexhibited">ausgestellte</a><br />
Exportiere: 
	<a href="export.php?picture_list_exhibited">alle</a> &bull;
	<a href="export.php?picture_list_exhibited&amp;exhibition_id=<?php echo $_GET['exhibition_id'] ?>">nur ausgestellte</a><br />
	
Drucken: 
	<a href="picture_list_exhibition_print.php?exhibition_id=<?php echo $_GET['exhibition_id'] ?>">Auslagenliste</a>
</p>

<form action="?p=picture_list_exhibited&amp;exhibition_id=<?php echo $_GET['exhibition_id'] ?>" method="post" id="tableform">
<table>
	<tr class="table_head">
		<td>Bild</td>
		<td>Details</td>
		<td>Optionen</td>
	</tr>
	
		<?php
			if(isset($_GET['onlyexhibited'])) {
				$result = $db->query("SELECT *,picture.id AS id FROM picture JOIN exhibited ON exhibited.exhibition_id='".$_GET['exhibition_id']."' AND exhibited.picture_id=picture.id ORDER BY picture.sortid DESC");
			} else {
				$result = $db->query("SELECT * FROM picture ORDER BY sortid DESC");
			}
			
			if (!$result) {
				echo "Anfrage konnte nicht ausgeführt werden: " . $db->lastErrorMsg();
				exit;
			}
			
			if ($db->querySingle("SELECT COUNT(*) FROM picture") == 0) {
				echo "Keine Zeilen gefunden, nichts auszugeben, daher Abbruch";
				exit;
			}
			
			//Hauptschleife
			while ($picture = $result->fetchArray()) {
				// look if picture is exhibited here
				$exhibitionRowCount = $db->querySingle("SELECT COUNT(*) FROM exhibited WHERE picture_id='".$picture['id']."' AND exhibition_id='".$_GET['exhibition_id']."'");
				if($exhibitionRowCount != 0) {
					$exhibitedQuery = $db->query("SELECT * FROM exhibited WHERE picture_id=".$picture['id']." AND exhibition_id='".$_GET['exhibition_id']."'");
					$exhibited = $exhibitedQuery->fetchArray();
					
					$isExhibitedHere = true;
					$price = $exhibited['price'];
					
					echo '<tr class="tr_green">';
				} else {
					echo "<tr>";
					$isExhibitedHere = false;	
				}
				
				
				$sql	= "SELECT COUNT(*) FROM exhibited JOIN exhibition ON exhibited.exhibition_id=exhibition.id AND exhibited.picture_id=".$picture['id']." "
							. "WHERE (exhibition.date_to == '' AND exhibition.date_from <= strftime('%s','now')) "
							. "OR (exhibition.date_to >= strftime('%s','now') AND exhibition.date_from <= strftime('%s','now'))";
				$isExhibitedATM = $db->querySingle($sql);
				
				echo '<td><a href="pictures/'.$picture['id'].'.jpg"><img src="thumbs/'.$picture['id'].'.jpg" class="thumbnail" title="'.$picture['name'].'" alt="'.$picture['name'].'" /></a>';
					echo "</td><td>";
					if($picture['sold'] == 1)
						echo '<img src="icons/reddot.png" width="10" height="10" style="border: 0" alt="verkauft" /> ';
					echo 'Titel: <b><a>'.$picture['name'].'</a></b>';
					echo "<br />Größe: ".$picture['width']."x".$picture['height']."cm (BxH)";
					echo "<br />Technik: ".$picture['technique'];
					echo "<br />Jahr: ".$picture['year'];
					
					// not sold
					if($picture['sold'] == false) {
						if ($isExhibitedHere)
							echo "<br />Aufbewahrungsort: <abbr title='Dieses Bild ist momentan ausgestellt. Sein Aufbewahrungsort wird wieder angezeigt, wenn es nicht mehr ausgestellt ist'>momentan ausgestellt</abbr>";
						else
							echo "<br />Aufbewahrungsort: ".$picture['location'];
							
						echo "<br />Richtpreis: ".$picture['avails']." €";
					}
					
					// sold
					if($picture['sold'] == true && $picture['avails'] != 0) {
						echo "<br /><br />Verkaufspreis: ".$picture['avails']."&euro;";
						echo "<br />Käufer: <abbr title='Tel.: ".$picture['buyer_phone']."\nE-Mail: ".$picture['buyer_email']."\nStadt: ".$picture['buyer_town']."'>".$picture['buyer_name']."</abbr>";
					}
					
					// given away
					if($picture['sold'] == true && $picture['avails'] == 0) {
						echo "<br /><br />Verschenkt an: <abbr title='Tel.: ".$picture['buyer_phone']."\nE-Mail: ".$picture['buyer_email']."\nStadt: ".$picture['buyer_town']."'>".$picture['buyer_name']."</abbr>";
					}
				echo "</td>";
				
				echo "<td>";
					if(!$protected) {
						if($isExhibitedHere) {
							echo '<input type="checkbox" name="'.$picture['id'].'" checked="checked" />';
							echo '<input type="number" name="price_'.$picture['id'].'" value="'.$exhibited['price'].'" class="price" placeholder="Preis" type="number" />';
							echo '<input type="text" name="location_'.$picture['id'].'" value="'.$exhibited['location'].'" class="location" placeholder="Ort/Zimmer" type="text" />';
							echo '<input type="submit" name="submit" value="OK" />';
						} else {
							echo '<input type="checkbox" name="'.$picture['id'].'" />';
							echo '<input type="number" name="price_'.$picture['id'].'" value="'.$picture['avails'].'" class="price" placeholder="Preis" type="number" />';
							echo '<input type="text" name="location_'.$picture['id'].'" placeholder="Ort/Zimmer" class="location" type="text" />';
							echo '<input type="submit" name="submit" value="OK" />';
						}
					} else {
						echo '<img src="icons/protect.png" alt="Geschützt" title="Ausstellung gesch&uuml;tzt, zum bearbeiten in der Ausstellungsliste den Schutz entfernen" alt="Gesch&uuml;tzt" class="protecticon" />';
					}
				echo "</td>";
				
				
				echo "</tr>";
			}
		?>
</table>
</form>
