<script type="text/javascript">
$(document).ready(function(){
	$(this).find("input:radio[name='status']").click(function() {
		if( $("input:radio:checked[name='status']").val() == "sold" ) {
			$('.buyer_info').show(0);
			$('.avails').show(0);
			$('.location').hide(0);
			$('#lbl_buyer_phone').text("Käufer Tel.");
			$('#lbl_buyer_email').text("Käufer EMail");
			$('#lbl_buyer_name').text("Käufer Name");
			$('#lbl_buyer_town').text("Käufer Ort");
			$('#lbl_avails').text("Kaufpreis");
		} else if ( $("input:radio:checked[name='status']").val() == "not_sold" ) {
			$('.buyer_info').hide(0);
			$('.avails').show(0);
			$('.location').show(0);
			$('#lbl_avails').text("Richtpreis");
		} else {
			$('.buyer_info').show(0);
			$('.avails').hide(0);
			$('.location').hide(0);
			$('#lbl_buyer_phone').text("Beschenkter Tel.");
			$('#lbl_buyer_email').text("Beschenkter EMail");
			$('#lbl_buyer_name').text("Beschenkter Name");
			$('#lbl_buyer_town').text("Beschenkter Ort");
		}
	});
});

$(document).ready(function(){
	if( $("input:radio:checked[name='status']").val() == "sold" ) {
		$('.buyer_info').show(0);
		$('.avails').show(0);
		$('.location').hide(0);
		$('#lbl_buyer_phone').text("Käufer Tel.");
		$('#lbl_buyer_email').text("Käufer EMail");
		$('#lbl_buyer_name').text("Käufer Name");
		$('#lbl_buyer_town').text("Käufer Ort");
		$('#lbl_avails').text("Kaufpreis");
	} else if ( $("input:radio:checked[name='status']").val() == "not_sold" ) {
		$('.buyer_info').hide(0);
		$('.avails').show(0);
		$('.location').show(0);
		$('#lbl_avails').text("Richtpreis");
	} else {
		$('.buyer_info').show(0);
		$('.avails').hide(0);
		$('.location').hide(0);
		$('#lbl_buyer_phone').text("Beschenkter Tel.");
		$('#lbl_buyer_email').text("Beschenkter EMail");
		$('#lbl_buyer_name').text("Beschenkter Name");
		$('#lbl_buyer_town').text("Beschenkter Ort");
	}
});
</script>



<?php
if ( $_SERVER['REQUEST_METHOD'] == "POST" ) {
	if( $_POST['status'] == "sold" ) {
		$sold = 1;
		$avails = $_POST['avails'];
		$buyer_name = $_POST['buyer_name'];
		$buyer_phone = $_POST['buyer_phone'];
		$buyer_town = $_POST['buyer_town'];
		$buyer_email = $_POST['buyer_email'];
		$location = "";
	} else if ( $_POST['status'] == "not_sold" ) {
		$sold = 0;
		$avails = $_POST['avails'];
		$buyer_name = "";
		$buyer_phone = "";
		$buyer_town = "";
		$buyer_email = "";
		$location = $_POST['location'];
	} else {
		$sold = 1;
		$avails = 0;
		$buyer_name = $_POST['buyer_name'];
		$buyer_phone = $_POST['buyer_phone'];
		$buyer_town = $_POST['buyer_town'];
		$buyer_email = $_POST['buyer_email'];
		$location = "";
	}


	$imgpath = "pictures/";
	$thumbpath = "thumbs/";
	$time = time();
	$fileerror = false;
	$dberror = false;
	$inputerror = false;
	
	if(empty($_POST['name']))
		$inputerror = "Bitte einen Namen angeben";
	
	//DB-Eintrag erstellen		
	if(!$inputerror){	
		$sortId = $db->querySingle("SELECT MAX(sortid) FROM picture");
		$sortId++;
		$sql = "INSERT INTO picture (name,width,height,year,avails,sold,technique,buyer_name,buyer_town,buyer_email,buyer_phone,location,sortid)"
					. "VALUES ('".$_POST['name']."','".$_POST['width']."','".$_POST['height']."','".$_POST['year']."','".$avails."','".$sold."','".$_POST['technique']."','".$buyer_name."','".$buyer_town."','".$buyer_email."','".$buyer_phone."','".$location."','".$sortId."')";
		$result = $db->query($sql);
		if (!$result){
			echo '<p><span class="error">Anfrage konnte nicht ausgeführt werden : ' . $db->lastErrorMsg() . '</p>';
			$dberror = "Fehler beim DB INSERT:<br />".$db->lastErrorMsg();
		}
	}
	
	//Bild hochladen
	if($_FILES['userfile']['size'] != 0 && !$dberror && !$inputerror){
		//ID des letzten DB eintrags abschnorcheln
		$id = $db->lastInsertRowid();
		$filename_img = $id.".jpg";
		
		if($_FILES['userfile']['type'] != $CONFIG['upload_mimetype'])
			$fileerror = "Bitte das Bild in eine jpg Datei umwandeln, dann erneut versuchen.";
		
		if($_FILES['userfile']['size'] > $CONFIG['upload_maxsize'])
			$fileerror = "Bild zu groß. Maximal 10MB erlaubt.";
			
		if(move_uploaded_file($_FILES['userfile']['tmp_name'], $imgpath.$filename_img)){
			if(!createthumb(200, 200, $imgpath.$filename_img, $thumbpath.$filename_img))
				$fileerror = "Vorschaubild konnte nicht erzeugt werden";
		}else{
			$fileerror = "Datei konnte nicht hochgeladen werden";
		}
		
		// DB Eintrag löschen falls upload in die Hose gegangen
		if($fileerror){
			$result = $db->query("DELETE FROM picture WHERE id='".$id."'");
			if(!$result)
				echo '<p class="error">Großer Mist, bitte jemand holen, der sich mit sowas auskennt.</p>';
		}
	}		
	
	if(!$dberror && !$fileerror && !$inputerror){
		echo '<p class="done">Bild erfolgreich eingetragen</p>';
	}else{
		echo '<p class="error">Fehler!<br />'.$inputerror.' '.$fileerror.' '.$dberror.'</p>';
	}
}
?>
	
<h2>Neues Bild eintragen</h2>
<form method="post" enctype="multipart/form-data" action="?p=picture_add">
	<fieldset>
		<label>Bild auswählen</label>
		<input name="userfile" type="file" accept="image/jpeg" />
	
		<label>Titel *</label>
		<input name="name" type="text" required autofocus  />
		
		<label>Größe</label>
		<input name="width" type="text" maxlength="3" class="measures" />x<input name="height" type="text" size="3" maxlength="3" class="measures"  /> (BxH)
		
		<label>Jahr</label>
		<input name="year" type="text" maxlength="4" />
		
		<label>Technik</label>
		<input name="technique" type="text" />

		<input type="radio" name="status" value="not_sold" id="not_sold" checked /><label for="not_sold" class="radiolabel" >nicht verkauft</label><br />
		<input type="radio" name="status" value="sold" id="sold" /><label for="sold" class="radiolabel" >verkauft</label><br />
		<input type="radio" name="status" value="given_away" id="given_away" /><label for="given_away" class="radiolabel" >verschenkt</label>
			
	</fieldset>
	
	<fieldset>
		<label class="location">Aufbewahrungsort<br /><small class="location">Befindet sich das Bild momentan auf einer Ausstellung,<br />so wird es als ausgestellt angezeigt</small>	</label>
		<input name="location" class="location" type="text" />
	
		<label class="avails" id="lbl_avails">Kaufpreis</label>
		<input name="avails" class="avails" type="number" placeholder="€" />
		
		<label class="buyer_info" id="lbl_buyer_name">Käufer Name</label>
		<input name="buyer_name" class="buyer_info" type="text" />
		
		<label class="buyer_info" id="lbl_buyer_town">Käufer Ort</label>
		<input name="buyer_town" class="buyer_info" type="text" />
		
		<label class="buyer_info" id="lbl_buyer_email">Käufer EMail</label>
		<input name="buyer_email" class="buyer_info" type="email" />
		
		<label class="buyer_info" id="lbl_buyer_phone">Käufer Telefon</label>
		<input name="buyer_phone" class="buyer_info" type="tel" />
	</fieldset>
	
	<fieldset>
		<input type="submit" name="submit_new" value="Speichern" />
	</fieldset>
</form>