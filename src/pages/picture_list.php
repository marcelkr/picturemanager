<script type="text/javascript">
$(document).ready(function () {
	$(".deleteicon").click(function() {
		var answer = confirm(unescape("Soll das Bild unwiederrufbar gel%F6scht werden%3F"));
		var picid = this.id;
		picid = picid.replace("del_","");
		if (answer){
			$.ajax({
				type: "POST",
				url: "scripts/picture_delete.php",
				data: {id: picid},
				success: function(response) {
					if(response == "OK") {
						$("#tr_" + picid).hide(800);
					}else{
						alert("Ein Fehler ist aufgetreten:\n" + response)
					}
				}
			});
		}
	});
});

$(document).ready(function () {
	$('.upicon').click(function (){
		var picid = this.id;
		picid = picid.replace("up_","");
		$.ajax({
				type: "POST",
				url: "scripts/picture_swap.php",
				data: {upSwapId: picid},
				success: function(response) {
					if(response == "OK") {
						$("#tr_"+picid).prev().before($("#tr_"+picid));
					}else{
						alert("Ein Fehler ist aufgetreten:\n" + response)
					}
				}
		});
	});
});
	
$(document).ready(function () {
	$('.downicon').click(function (){
		var picid = this.id;
		picid = picid.replace("down_","");
		$.ajax({
				type: "POST",
				url: "scripts/picture_swap.php",
				data: {downSwapId: picid},
				success: function(response) {
					if(response == "OK") {
						$("#tr_"+picid).next().after($("#tr_"+picid));
					}else{
						alert("Ein Fehler ist aufgetreten:\n" + response)
					}
				}
		});
	});
});

$(document).ready(function () {
	$('#biggerThumbs').click(function (){
		$('.thumbnail').css("max-height","+=50px");
		$('.thumbnail').css("max-width","+=50px");
	});
});

$(document).ready(function () {
	$('#smallerThumbs').click(function (){
		$('.thumbnail').css("max-height","-=50px");
		$('.thumbnail').css("max-width","-=50px");
	});
});
</script>

<p>
Zeige ausschließlich: <a href="index.php?p=picture_list">alle</a> &bull;
	<!--<a href="index.php?p=picture_list&amp;onlyexhibited">ausgestellte</a> &bull;
	<a href="index.php?p=picture_list&amp;notexhibited">nicht ausgestellte</a> &bull;-->
	<a href="index.php?p=picture_list&amp;sold">verkaufte</a> &bull;
	<a href="index.php?p=picture_list&amp;notsold">nicht verkaufte</a> &bull;
	<a href="index.php?p=picture_list&amp;givenaway">verschenkte</a><br />
Exportiere: <a href="export.php?picture_list">alle</a><br />
Vorschaubilder: 
	<img src="icons/loupe_bigger.png" class="loupeicon" id="biggerThumbs" alt="Vorschaubilder vergrößern" title="Vorschaubilder vergrößern" />
	<img src="icons/loupe_smaller.png" class="loupeicon" id="smallerThumbs" alt="Vorschaubilder verkleinern" title="Vorschaubilder verkleinern"/><br />
Ansicht: 
	<a href="index.php?p=picture_list"><img src="icons/list.png" class="listicon" alt="Listenansicht" /></a> 
	<a href="index.php?p=picture_tiles"><img src="icons/tiles.png" class="tilesicon" alt="Kachelansicht"/></a>
</p>

<table>
	<tr class="table_head">
		<td>Bild</td>
		<td>Details</td>
		<td>Ausstellungen</td>
		<td>Optionen</td>
	</tr>
	
<?php
if(isset($_GET['onlyexhibited'])) {
	$sql = "SELECT picture.id AS id, picture.sold AS sold, picture.name AS name, picture.width AS width, picture.height AS height, picture.technique AS technique, picture.year AS year, picture.buyer_phone AS buyer_phone, picture.buyer_name AS buyer_name, picture.buyer_email AS buyer_email, picture.buyer_town AS buyer_town, picture.avails AS avails, exhibition.date_from, exhibition.date_to\n"
    		. "FROM picture "
    		. "JOIN exhibited ON picture.id = exhibited.picture_id "
    		. "JOIN exhibition ON exhibition.id = exhibited.exhibition_id "
    		. "WHERE (exhibition.date_from <= strftime('%s','now') "
    		. "AND exhibition.date_to >= strftime('%s','now')) "
    		. "OR exhibition.date_to = '0'"
    		. "ORDER BY sortid  DESC";
} else if (isset($_GET['notexhibited'])) {
	$sql = "SELECT picture.id AS id, picture.sold AS sold, picture.name AS name, picture.width AS width, picture.height AS height, picture.technique AS technique, picture.year AS year, picture.buyer_phone AS buyer_phone, picture.buyer_name AS buyer_name, picture.buyer_email AS buyer_email, picture.buyer_town AS buyer_town, picture.avails AS avails, exhibition.date_from, exhibition.date_to\n"
    		. "FROM picture\n"
    		. "JOIN exhibited ON picture.id = exhibited.picture_id "
    		. "JOIN exhibition ON exhibition.id = exhibited.exhibition_id "
    		. "WHERE (exhibition.date_from <= strftime('%s','now') "
    		. "AND exhibition.date_to <= strftime('%s','now'))"
    		. "ORDER BY sortid  DESC";
} else if (isset($_GET['sold'])) {
	$sql = "SELECT * FROM picture WHERE sold != 0 and avails != 0 ORDER BY sortid DESC";
} else if (isset($_GET['notsold'])) {
	$sql = "SELECT * FROM picture WHERE sold = 0 ORDER BY sortid DESC";
} else if (isset($_GET['givenaway'])) {
	$sql = "SELECT * FROM picture WHERE sold = 1 AND avails = 0 ORDER BY sortid DESC";
} else {
	$sql = "SELECT * FROM picture ORDER BY sortid DESC";
}
$pictureQuery = $db->query($sql);

if (!$pictureQuery) {
	echo "Anfrage konnte nicht ausgeführt werden: " . $db->lastErrorMsg();
	exit;
}

if ($db->querySingle("SELECT COUNT(*) FROM picture") == 0) {
	echo "Keine Zeilen gefunden, nichts auszugeben, daher Abbruch";
	exit;
}

//Hauptschleife
while ($picture = $pictureQuery->fetchArray()) {
	$sql	= "SELECT COUNT(*) FROM exhibited JOIN exhibition ON exhibited.exhibition_id=exhibition.id AND exhibited.picture_id=".$picture['id']." "
				. "WHERE (exhibition.date_to == '' AND exhibition.date_from <= strftime('%s','now')) "
				. "OR (exhibition.date_to >= strftime('%s','now') AND exhibition.date_from <= strftime('%s','now'))";
	$isExhibitedATM = $db->querySingle($sql);
	
	if(file_exists("thumbs/".$picture['id'].".jpg"))
		echo '<tr id="tr_'.$picture['id'].'"><td><a href="pictures/'.$picture['id'].'.jpg"><img src="thumbs/'.$picture['id'].'.jpg" class="thumbnail" title="'.$picture['name'].'" alt="'.$picture['name'].'" /></a>';
	else
		echo '<tr id="tr_'.$picture['id'].'"><td><img src="icons/questionmark.png" class="thumbnail" title="Kein Bild hochgeladen" alt="Kein Bild hochgeladen" />';
	echo "</td><td>";
	if($picture['sold'] == true)
		echo '<img src="icons/reddot.png" class="reddot" alt="verkauft" /> ';
	echo 'Titel: <b>'.$picture['name'].'</b>';
	echo "<br />Größe: ".$picture['width']."x".$picture['height']."cm (BxH)";
	echo "<br />Technik: ".$picture['technique'];
	echo "<br />Jahr: ".$picture['year'];
	
	// not sold
	if($picture['sold'] == false) {
		if ($isExhibitedATM)
			echo "<br />Aufbewahrungsort: <abbr title='Dieses Bild ist momentan ausgestellt. Sein Aufbewahrungsort wird wieder angezeigt, wenn es nicht mehr ausgestellt ist'>momentan ausgestellt</abbr>";
		else
			echo "<br />Aufbewahrungsort: ".$picture['location'];
			
		echo "<br />Richtpreis: ".$picture['avails']." €";
	}
	
	// sold
	if($picture['sold'] == true && $picture['avails'] != 0) {
		echo "<br /><br />Verkaufspreis: ".$picture['avails']."&euro;";
		echo "<br />Käufer: <abbr title='Tel.: ".$picture['buyer_phone']."\nE-Mail: ".$picture['buyer_email']."\nStadt: ".$picture['buyer_town']."'>".$picture['buyer_name']."</abbr>";
	}
	
	// given away
	if($picture['sold'] == true && $picture['avails'] == 0) {
		echo "<br /><br />Verschenkt an: <abbr title='Tel.: ".$picture['buyer_phone']."\nE-Mail: ".$picture['buyer_email']."\nStadt: ".$picture['buyer_town']."'>".$picture['buyer_name']."</abbr>";
	}
	echo "</td>";
	
	echo "<td>";
	$exhibitionQuery = $db->query("SELECT exhibition.* FROM exhibited JOIN exhibition ON exhibited.exhibition_id=exhibition.id AND exhibited.picture_id='".$picture['id']."' ORDER BY exhibition.date_from");
	while ($exhibition = $exhibitionQuery->fetchArray()) {
		if ($exhibition['date_to'] != "")
			echo date('d.m.Y',$exhibition['date_from']).' - '.date('d.m.Y',$exhibition['date_to']).' <b>"'.$exhibition['title'].'"</b>, '.$exhibition['location'].'<br />';
		else
			echo date('d.m.Y',$exhibition['date_from']).' - ? <b>"'.$exhibition['title'].'"</b>, '.$exhibition['location'].'<br />';
	}
	echo "</td>";
	
	echo "<td>";
	echo '<a href="?p=picture_edit&amp;edit='.$picture['id'].'"><img src="icons/edit.png" class="editicon" title="Bearbeiten" alt="Bearbeiten" /></a>';
	echo '<img src="icons/delete.png" title="Löschen" alt="Löschen" class="deleteicon" id="del_'.$picture['id'].'" />';
	echo '<img src="icons/up.png" title="Hoch" alt="Hoch" class="upicon" id="up_'.$picture['id'].'" />';
	echo '<img src="icons/down.png" title="Herunter" alt="Herunter" class="downicon" id="down_'.$picture['id'].'" />';
	echo "</td>";
	echo "</tr>";
}
?>
</table>
