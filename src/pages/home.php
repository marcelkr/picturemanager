<h2>Ausstellungen</h2>
<?php
echo "<p>Ausstellungen gesamt: ".$db->querySingle("SELECT COUNT(*) FROM exhibition")."</p>";
?>

<h2>Bilder</h2>
<?php
$result = $db->query("SELECT avails FROM picture WHERE sold=1");
$earnings = 0;
$result = $db->query("SELECT * FROM picture");
while ($row = $result->fetchArray())
	$earnings += (float)$row['avails'];

echo "<p>Bilder gesamt: ".$db->querySingle("SELECT COUNT(*) FROM picture")."<br />";
echo "Bilder verkauft: ".$db->querySingle("SELECT COUNT(*) FROM picture WHERE sold=1")."<br />";
echo "Einnahmen: ".$earnings."&euro;</p>";
?>

<h2>Personen</h2>
<?php
echo "<p>Personen gesamt: ".$db->querySingle("SELECT COUNT(*) FROM person")."</p>";
?>
