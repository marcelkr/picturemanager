<?php
// Zu einer AUsstellung hinzufügen
if(isset($_GET['add'])) {
	$rowCount = $db->querySingle("SELECT COUNT(*) FROM exhibited WHERE picture_id='".$_GET['add']."' AND exhibition_id='".$_GET['exhibition_id']."'");
	if($rowCount == 0) {	// the picture is not added yet
		$result = $db->query("INSERT INTO exhibited (exhibition_id ,picture_id) VALUES ('".$_GET['exhibition_id']."', '".$_GET['add']."')");
		if (!$result) {
			echo '<p class="error">Bild konnte nicht hinzugefügt werden:<br />' . $db->lastErrorMsg() . '</p>';
			exit;
		}
	}
}

// Von einer Ausstellung löschen
if(isset($_GET['remove'])) {
	$result = $db->query("DELETE FROM exhibited WHERE picture_id=".$_GET['remove']." AND exhibition_id=".$_GET['exhibition_id']);
	if (!$result) {
		echo '<p class="error">Bild konnte nicht von der Ausstellung entfernt werden: ' . $db->lastErrorMsg() . '</p>';
		exit;
	}
}

if(isset($_GET['exhibition_id'])) {
	$result = $db->query("SELECT * FROM exhibited WHERE exhibition_id=".$_GET['exhibition_id']);
	$rowCount = $db->querySingle("SELECT COUNT(*) FROM exhibited WHERE exhibition_id=".$_GET['exhibition_id']);
	
	$row2 = $db->query("SELECT * FROM exhibition WHERE id='".$_GET['exhibition_id']."'")->fetchArray();
	$protected = $row2['protected'];
	
	echo '<h2>Bilderliste für: "'.$row2['title'].'"</h2>';
	echo "<p><b>".$row2['location']."</b><br />".$rowCount." Bilder auf dieser Ausstellung</p>";
}
?>

<p>
<a href="index.php?p=picture_tiles&amp;exhibition_id=<?php echo $_GET['exhibition_id'] ?>&amp;onlyexhibited">Zeige nur ausgestellte</a> &bull;
<a href="index.php?p=picture_tiles&amp;exhibition_id=<?php echo $_GET['exhibition_id'] ?>">Zeige alle</a><br />
<a href="export.php?picture_list">Exportiere alle</a> &bull;
<a href="export.php?picture_list&amp;exhibition_id=<?php echo $_GET['exhibition_id'] ?>">exportiere nur ausgestellte</a>
</p>

<table>	
<?php
if(!isset($_GET['onlyexhibited'])) {
	$result = $db->query("SELECT * FROM picture ORDER BY id DESC");
	$rowCount = $db->querySingle("SELECT COUNT(*) FROM picture ORDER BY id DESC");
}else{
	$result = $db->query("SELECT picture.id,picture.* FROM picture JOIN exhibited ON picture.id = exhibited.picture_id WHERE exhibited.exhibition_id=".$_GET['exhibition_id']." ORDER BY picture.id DESC");		
	$rowCount = $db->querySingle("SELECT COUNT(*) FROM picture JOIN exhibited ON picture.id = exhibited.picture_id WHERE exhibited.exhibition_id=".$_GET['exhibition_id']." ORDER BY picture.id DESC");
}

if (!$result) {
	echo "Anfrage konnte nicht ausgeführt werden: " . $db->lastErrorMsg();
	exit;
}

if ($rowCount == 0) {
	echo "Keine Bilder eingetragen";
	exit;
}

//Hauptschleife
$row = $result->fetchArray();
while ($row) {
	echo '<tr>';
	for($i=0; $i<=2 && $row != false; $i++, $row = $result->fetchArray()){
		if(isset($_GET['exhibition_id'])) {
			$rowCount = $db->querySingle("SELECT COUNT(*) FROM exhibited WHERE picture_id=".$row['id']." AND exhibition_id=".$_GET['exhibition_id']);
				
			$exhibited = false;
			if($rowCount != 0) {
				$exhibited = true;
				echo '<td class="td_tiles_green">';
			}else {
				echo '<td class="td_tiles">';
			}
		}else {
			echo '<td class="td_tiles">';
		}
		
		echo '<a href="pictures/'.$row['id'].'.jpg"><img src="thumbs/'.$row['id'].'.jpg" style="border: 0" /></a>';
		if($row['sold'] == true)
			echo '<img class="reddot" src="icons/reddot.png" width="16" height="16" style="border: 0" alt="verkauft" />';
		echo '<br  style="clear:both;" />';
		echo '<b><a style="clear:both;" name="'.$row['id'].'">'.$row['name'].'</b> ';
		
		
		// Buttons zum hinzufügen/entfernen zu gegebener Ausstellung
		if(isset($_GET['exhibition_id'])) {
			if(!$protected) {
				if($exhibited) {
					echo '<a href="?p=picture_tiles&amp;exhibition_id='.$_GET['exhibition_id'].'&amp;remove='.$row['id'].'">';
					echo '<img src="icons/remove.png" height="20" width="20" style="border: 0" title="Von dieser Ausstellung entfernen" alt="Entfernen" /></a>';
				} else {
					echo '<a href="?p=picture_tiles&amp;exhibition_id='.$_GET['exhibition_id'].'&amp;add='.$row['id'].'">';
					echo '<img src="icons/add.png" height="20" width="20" style="border: 0" title="Zu dieser Ausstellung hinzuf&uuml;gen" alt="Hinzuf&uuml;gen" /></a>';
				}
			} else {
				echo '<img src="icons/protect.png" height="20" width="20" title="Ausstellung gesch&uuml;tzt, zum bearbeiten in der Ausstellungsliste den Schutz entfernen" alt="Gesch&uuml;tzt" />';
			}
		} else {
			echo '<a href="?p=picture_edit&amp;edit='.$row['id'].'"><img src="icons/edit.png" height="20" width="20" style="border: 0" title="Bearbeiten" alt="Bearbeiten" /></a> <a href="?p=picture_list&amp;delete='.$row['id'].'"><img src="icons/delete.png" height="20" width="20" style="border: 0" title="L&ouml;schen" alt="L&ouml;schen" /></a>';
		}
			
		echo '</td>';	
	}




echo '</tr>';
}


				//Liste für bestimmte Ausstellung
				/*if(isset($_GET['exhibition_id'])) {
					$result2 = $db->query("SELECT * FROM exhibited WHERE picture_id=".$row['id']." AND exhibition_id=".$_GET['exhibition_id']);
						
					$exhibited = false;
					if($result2->numRows() != 0) {
						$exhibited = true;
						echo '<tr class="tr_green">';
					}
				}
				
				
				echo '<td><a href="picture_detail.php?id='.$row['id'].'"><img src="thumbs/'.$imgpath.'" style="border: 0" /></a></td>';
				echo "<td>";
				if($row['sold'] == true)
					echo '<img src="icons/reddot.png" width="16" height="16" style="border: 0" alt="verkauft" /><br />';
				echo 'Name: <b><a name="'.$row['id'].'">'.$row['name'].'</b>';
				echo "<br />Größe: ".$row['width']."x".$row['height']."cm (BxH)";
				echo "<br />Technik: ".$row['technique'];
				echo "<br />Jahr: ".$row['year'];
				echo "<br />".$row['price']."&euro;";
				echo "</td>";
				
				echo "<td>";
				$result3 = $db->query("SELECT exhibition.* FROM exhibited JOIN exhibition ON exhibited.exhibition_id=exhibition.id AND exhibited.picture_id=".$row['id']);
				while ($row3 = sqlite_array_query($result3)) {
					echo date('d.m.Y',$row3['date_from']).' - '.date('d.m.Y',$row3['date_to']).' <b>"'.$row3['title'].'"</b>, '.$row3['location'].'<br />';
				}
				echo "</td>";
				
				echo "<td>";
				if(isset($_GET['exhibition_id'])) {
					if(!$protected) {
						if($exhibited)
							echo '<a href="?p=picture_list&amp;exhibition_id='.$_GET['exhibition_id'].'&amp;remove='.$row['id'].'"><img src="icons/remove.png" height="20" width="20" style="border: 0" title="Von dieser Ausstellung entfernen" alt="Entfernen" /></a>';
						else
							echo '<a href="?p=picture_list&amp;exhibition_id='.$_GET['exhibition_id'].'&amp;add='.$row['id'].'"><img src="icons/add.png" height="20" width="20" style="border: 0" title="Zu dieser Ausstellung hinzuf&uuml;gen" alt="Hinzuf&uuml;gen" /></a>';
					} else {
						echo '<img src="icons/protect.png" height="20" width="20" style="border: 0" title="Ausstellung gesch&uuml;tzt, zum bearbeiten in der Ausstellungsliste den Schutz entfernen" alt="Gesch&uuml;tzt" />';
					}
				} else {
					echo '<a href="?p=picture_edit&amp;edit='.$row['id'].'"><img src="icons/edit.png" height="20" width="20" style="border: 0" title="Bearbeiten" alt="Bearbeiten" /></a> <a href="?p=picture_list&amp;delete='.$row['id'].'"><img src="icons/delete.png" height="20" width="20" style="border: 0" title="L&ouml;schen" alt="L&ouml;schen" /></a>';
				}
				
				echo "</td>";
				echo "</tr>";
			}*/
		?>
</table>
