<script type="text/javascript">
$(document).ready(function(){
	$(this).find("input:radio[name='status']").click(function() {
		if( $("input:radio:checked[name='status']").val() == "sold" ) {
			$('.buyer_info').show(0);
			$('.avails').show(0);
			$('.location').hide(0);
			$('#lbl_buyer_phone').text("Käufer Tel.");
			$('#lbl_buyer_email').text("Käufer EMail");
			$('#lbl_buyer_name').text("Käufer Name");
			$('#lbl_buyer_town').text("Käufer Ort");
			$('#lbl_avails').text("Kaufpreis");
		} else if ( $("input:radio:checked[name='status']").val() == "not_sold" ) {
			$('.buyer_info').hide(0);
			$('.avails').show(0);
			$('.location').show(0);
			$('#lbl_avails').text("Richtpreis");
		} else {
			$('.buyer_info').show(0);
			$('.avails').hide(0);
			$('.location').hide(0);
			$('#lbl_buyer_phone').text("Beschenkter Tel.");
			$('#lbl_buyer_email').text("Beschenkter EMail");
			$('#lbl_buyer_name').text("Beschenkter Name");
			$('#lbl_buyer_town').text("Beschenkter Ort");
		}
	});
});

$(document).ready(function(){
	if( $("input:radio:checked[name='status']").val() == "sold" ) {
		$('.buyer_info').show(0);
		$('.avails').show(0);
		$('.location').hide(0);
		$('#lbl_buyer_phone').text("Käufer Tel.");
		$('#lbl_buyer_email').text("Käufer EMail");
		$('#lbl_buyer_name').text("Käufer Name");
		$('#lbl_buyer_town').text("Käufer Ort");
		$('#lbl_avails').text("Kaufpreis");
	} else if ( $("input:radio:checked[name='status']").val() == "not_sold" ) {
		$('.buyer_info').hide(0);
		$('.avails').show(0);
		$('.location').show(0);
		$('#lbl_avails').text("Richtpreis");
	} else {
		$('.buyer_info').show(0);
		$('.avails').hide(0);
		$('.location').hide(0);
		$('#lbl_buyer_phone').text("Beschenkter Tel.");
		$('#lbl_buyer_email').text("Beschenkter EMail");
		$('#lbl_buyer_name').text("Beschenkter Name");
		$('#lbl_buyer_town').text("Beschenkter Ort");
	}
});
</script>


<?php
if($_SERVER['REQUEST_METHOD'] == "POST") {
	if( $_POST['status'] == "sold" ) {
		$sold = 1;
		$avails = $_POST['avails'];
		$buyer_name = $_POST['buyer_name'];
		$buyer_phone = $_POST['buyer_phone'];
		$buyer_town = $_POST['buyer_town'];
		$buyer_email = $_POST['buyer_email'];
		$location = "";
	} else if ( $_POST['status'] == "not_sold" ) {
		$sold = 0;
		$avails = $_POST['avails'];
		$buyer_name = "";
		$buyer_phone = "";
		$buyer_town = "";
		$buyer_email = "";
		$location = $_POST['location'];
	} else {
		$sold = 1;
		$avails = 0;
		$buyer_name = $_POST['buyer_name'];
		$buyer_phone = $_POST['buyer_phone'];
		$buyer_town = $_POST['buyer_town'];
		$buyer_email = $_POST['buyer_email'];
		$location = "";
	}

	//DB Eintrag updaten
	$error = false;
	$imgpath = "pictures/";
	$thumbpath = "thumbs/";
	
	$result = $db->query("UPDATE picture SET name='".$_POST['name']."',year='".$_POST['year']."',avails='".$avails."',width='".$_POST['width']."',height='".$_POST['height']."',year='".$_POST['year']."',sold='".$sold."',technique='".$_POST['technique']."',buyer_name='".$buyer_name."',buyer_town='".$buyer_town."',buyer_email='".$buyer_email."',buyer_phone='".$buyer_phone."',location='".$location."' WHERE id='".$_GET['edit']."'");
	if (!$result) {
		$error = true;
	}
	
	if($_FILES['userfile']['size'] != 0 && !$error) {
		echo "NEW FILE UPLOAD";	
		// upload new pic
		$id = $_GET['edit'];
		$filename_img = $id.".jpg";
		
		if($_FILES['userfile']['type'] != $CONFIG['upload_mimetype']) {
			$fileerror = "Bitte das Bild in eine jpg Datei umwandeln, dann erneut versuchen.";
			$error = true;
		}
		
		if($_FILES['userfile']['size'] > $CONFIG['upload_maxsize']) {
			$fileerror = "Bild zu groß. Maximal 10MB erlaubt.";
			$error = true;
		}
		
		if(!$error) {
			// rename old file			
			if(file_exists("../pictures/".$id.".jpg"))
				rename("../pictures/".$id.".jpg", "../pictures/".$id.".jpg_tmp");
			if(file_exists("../thumbs/".$id.".jpg"))
				rename("../thumbs/".$id.".jpg", "../thumbs/".$id.".jpg_tmp");
			
			if(move_uploaded_file($_FILES['userfile']['tmp_name'], "pictures/".$filename_img)){
				if(createthumb(200, 200, $imgpath.$filename_img, $thumbpath.$filename_img)) {
					// delete old pics
					if(file_exists("../pictures/".$id.".jpg_tmp"))
						$unlinkpic = unlink("../pictures/".$id.".jpg");
					if(file_exists("../thumbs/".$id.".jpg"))
						$unlinkthumb = unlink("../thumbs/".$id.".jpg_tmp");
				} else {
					// restore old pic
					rename("../pictures/".$id.".jpg_tmp", "../pictures/".$id.".jpg");
					rename("../thumbs/".$id.".jpg_tmp", "../thumbs/".$id.".jpg");		
					$error = true;
				}
			}else{
				$error = true;
			}
		}
	}
	
	if ($error)
		echo '<p class="error">Ein Fehler ist aufgetreten!</p>';
	else
		echo '<p class="done">Eintrag geändert!</p>';
}

$pictureQuery = $db->query("SELECT * FROM picture WHERE id='".$_GET['edit']."'");
$picture = $pictureQuery->fetchArray();
?>
	
<h2>Bild ändern</h2>

<form method="post" enctype="multipart/form-data" action="?p=picture_edit&amp;edit=<?php echo $_GET['edit'] ?>">
	<fieldset>
		<label>Bild auswählen</label>
		<input name="userfile" type="file" accept="image/jpeg" />
	
		<label>Titel *</label>
		<input name="name" type="text" value="<?php echo $picture['name'] ?>" required autofocus  />
		
		<label>Größe</label>
		<input name="width" type="text" maxlength="3" value="<?php echo $picture['width'] ?>" class="measures"  />x<input name="height" type="text" size="3" maxlength="3" value="<?php echo $picture['height'] ?>" class="measures"  /> (BxH)
		
		<label>Jahr</label>
		<input name="year" type="text" maxlength="4" value="<?php echo $picture['year'] ?>" />
		
		<label>Technik</label>
		<input name="technique" type="text" value="<?php echo $picture['technique'] ?>" />

		<input type="radio" name="status" value="not_sold" id="not_sold" <?php if($picture['sold'] == 0) {echo 'checked';} ?> /><label for="not_sold" class="radiolabel" >nicht verkauft</label><br />
		<input type="radio" name="status" value="sold" id="sold" <?php if($picture['sold'] == 1 && $picture['avails'] != 0) {echo 'checked';} ?> /><label for="sold" class="radiolabel" >verkauft</label><br />
		<input type="radio" name="status" value="given_away" id="given_away" <?php if($picture['sold'] == 1 && $picture['avails'] == 0) {echo 'checked';} ?> /><label for="given_away" class="radiolabel" >verschenkt</label>
			
	</fieldset>
	
	<fieldset>
		<label class="location">Aufbewahrungsort<br /><small class="location">Befindet sich das Bild momentan auf einer Ausstellung,<br />so wird es als ausgestellt angezeigt</small>	</label>
		<input name="location" class="location" type="text" value="<?php echo $picture['location'] ?>" />
	
		<label class="avails" id="lbl_avails">Kaufpreis</label>
		<input name="avails" class="avails" type="number" value="<?php echo $picture['avails'] ?>" placeholder="€" />
		
		<label class="buyer_info" id="lbl_buyer_name">Käufer Name</label>
		<input name="buyer_name" class="buyer_info" type="text" value="<?php echo $picture['buyer_name'] ?>" />
		
		<label class="buyer_info" id="lbl_buyer_town">Käufer Ort</label>
		<input name="buyer_town" class="buyer_info" type="text" value="<?php echo $picture['buyer_town'] ?>" />
		
		<label class="buyer_info" id="lbl_buyer_email">Käufer EMail</label>
		<input name="buyer_email" class="buyer_info" type="email" value="<?php echo $picture['buyer_email'] ?>" />
		
		<label class="buyer_info" id="lbl_buyer_phone">Käufer Telefon</label>
		<input name="buyer_phone" class="buyer_info" type="tel" value="<?php echo $picture['buyer_phone'] ?>" />
	</fieldset>
	
	<fieldset>
		<input type="submit" name="submit_new" value="Speichern" />
	</fieldset>
</form>
