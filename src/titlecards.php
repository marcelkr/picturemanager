<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>Titelkarten</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta name="generator" content="Bluefish 2.2.3" />
	<link rel="stylesheet" type="text/css" href="titlecards.css" />
</head>
<body>

<?php
require_once("config.php");
$db = new SQLite3($CONFIG['sqlite_file']);


if(isset($_GET['exhibition_id'])) {
	$result = $db->query("SELECT protected FROM exhibition WHERE rowid='".$_GET['exhibition_id']."'");
	$row = $db->fetchArray();
	$protected = $row['protected'];
}
?>

<table>	
	<?php
		$result = $db->query("SELECT picture.* FROM picture JOIN exhibited ON exhibited.picture_id=picture.rowid WHERE exhibited.exhibition_id='".$_GET['exhibition_id']."'");
		
		if (!$result) {
			echo "Anfrage konnte nicht ausgef&uuml;hrt werden : " . $db->lastErrorMsg();
			exit;
		}
		
		$rows = $result->numRows();
		if ($rows == 0) {
			echo "Keine Zeilen gefunden, nichts auszugeben, daher Abbruch";
			exit;
		}


		$tablerows = ceil($rows/2);
		
		for($i = 0; $i<$tablerows; $i++) {
			echo "<tr>";
			for($j = 0; $j<2; $j++) {
				$row = $db->fetchArray();
				echo "<td>";
				echo '<span style="font-size: 12pt;">Titel: <b>'.$row['name'].'</b></span>';
				echo "<br /><br />Gr&ouml;&szlig;e: ".$row['width']."x".$row['height']."cm (BxH)";
				echo "<br />Technik: ".$row['technique'];
				echo "<br />Lfd. Nr.: ".$row['technique'];
				echo "</td>";
			}
			echo "</tr>";
		}
	?>
</table>

<?php
$db->close();
?>

</body>
</html>
