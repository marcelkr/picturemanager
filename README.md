# Picturemanager

The Picturemanager is a software I wrote for my mum to manage her paintings. It keeps track of the paintings name but also where they're stored, where they were exhibited, if they're sold, for which price they sold, to whom they were sold etc. The picturemanager includes a way of tracking exhibitions and contacts.


![](doc/screenshot_2.png)

## Installation

Rename `picturemanager.sqlite.dist` to `picturemanager.sqlite`. Done.

Just use PHPs built in server to test the Picturemanager. Open a terminal, change to `src` and start the server via `php -S localhost:8000`.

## The little sins

I thought I lost the code of this project but just discovered it in some random folder on my notebook. This piece of software is about 10 years old according to the changelog. For me it's quite interesting to see what I've learned since then. Here's some things I'd do different today:

- In the Picturemanager you see $_POST variables directly inserted in SQL queries.
- PHP and HTML are intermixed. I'd now use a template engine (or rather a proper framework like Laravel or Symfony) to split the logic from the UI code
- The SQlite driver is used. Obviously PHP PDO should be used to allow using whatever DB one wants. 
- There's no code for intializing the DB.
- All the buyer information (name, address, mail, ...) is stored in the picture table. Normalizing this would be the obvious refactoring.
- jQuery. For the little interactive stuff there is, I'd use plain JS.
- I'd now use a proper icon set instead of some PNGs.

## The way forward
My mum still wants to use the picturemanager. Apart from desperately needing a fresh UI, it would probably a good idea to rewrite it and fix all my sins from above. But how? Making it a desktop application would be great. The obvious choice might be Electron but I'm not a big fan. Using Qt would mean a lot of learning since I only tinkered around with some example projects before. Rewriting it with Laravel or Symfony? Let's see.
